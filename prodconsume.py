import asyncio

import cv2
import numpy as np

import config


async def produce_from_file(queue, filename, simulate=False):
    video = cv2.VideoCapture(f"{filename}")
    while True:
        ret, frame = video.read()  # Lecture de la frame
        if ret:
            await queue.put(frame)
            # Simule la vitesse de production des frames une par une
            if simulate:
                await asyncio.sleep(1 / config.DICART_FPS)
            else:
                # On passe la main (éventuellement) au consommateur
                await asyncio.sleep(1e-16)

        else:
            await queue.put(None)
            break
    video.release()
    cv2.destroyAllWindows()


async def consume(queue):
    moyl = []
    moya = []
    shape = None
    while True:
        item = await queue.get()
        if item is None:  # fin de la vidéo
            break
        if shape is None:
            assert (item.shape[0] == config.DICART_HEIGHT)
            assert (item.shape[1] == config.DICART_WIDTH)
            shape = item.shape
        l, a = frameprocess(item)
        moyl.append(l)
        moya.append(a)
        queue.task_done()
        await asyncio.sleep(1e-16)  # On passe la main (éventuellement) au producteur
    print("pré-traitement des frames terminé")
    return moyl, moya


async def main_file(filename):
    queue = asyncio.Queue(maxsize=config.DICART_MAX_FRAMES)
    producer = produce_from_file(queue, filename, simulate=True)
    consumer = consume(queue)
    _, moyenne = await asyncio.gather(producer, consumer)
    return moyenne


def frameprocess(frame):
    '''
    Reduction de dimension de la frame et transformation de celle-ci dans l'espace couleur Lab. Ouverture d'un
    tebleau.npy et enregistrement dans celui-ci de la moyenne du canal L (servira pour la detection du piston) et de la moyenne
    du canal a (pour la detection du trc)

    Parametres
    ----------

    frame : numpy array
        image à analyser
    Returns
    --------
    meanlab[0]: float
        moyenne du canal l pour chaque frame

    meanlab[1]: float
        moyenne du canal a pour chaque frame
    '''
    # Différente transformation pour ne garder que la peau et réduire le nb de pixels pour chaque mode possible.
    # Garde environ 10 000 pixels pour chaque mode
    top, bot = config.BOUNDING_BOX_TOP, config.BOUNDING_BOX_BOTTOM
    left, right = config.BOUNDING_BOX_LEFT, config.BOUNDING_BOX_RIGHT
    framecrop = frame[top:bot:config.DECIMATION, left:right:config.DECIMATION, :]
    framelab = cv2.cvtColor(framecrop, cv2.COLOR_BGR2LAB)  # TODO:Attention BGR n'est pas RGB565
    meanlab = np.mean(framelab, axis=(0, 1))  # moyenne des canaux l,a et b sur la fenetre
    return meanlab[0], meanlab[1]
