# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 09:46:53 2021

@author: annes
"""

import numpy as np

import config


def gaussian_filter(signal, sigma):
    x_ = np.arange(int(np.floor(-3 * sigma)), int(np.ceil(3 * sigma)) + 1)
    filt = np.exp(-0.5 * x_ ** 2 / sigma ** 2) / np.sqrt(2. * np.pi * sigma ** 2)
    reflect = len(filt) // 2
    courbelisse = np.convolve(np.concatenate((signal[:reflect][::-1], signal,
                                              signal[-reflect:][::-1])),
                              filt,
                              mode='valid')
    return courbelisse


def get_ind_piston(mean_l, sigma_l):
    courbelisse = gaussian_filter(mean_l, sigma_l)
    gradient = np.gradient(courbelisse)

    pistonframe = np.argmax(gradient)

    return pistonframe


def estimation_trc(para_pos, window, sigma_l, sigma_a, mean_l, mean_a, ind_piston=None, para_neg=1):
    # Calcul indice piston
    ind_piston = get_ind_piston(mean_l, sigma_l) if ind_piston is None else ind_piston

    # Exploitation de la moyenne du canal a
    courbelisse = gaussian_filter(mean_a[ind_piston:], sigma_a)
    gradient_ = np.gradient(courbelisse)

    # Moyenne des gradiens positifs, moyenne des gradients négatifs
    mpos_grad = np.mean(gradient_[np.where(gradient_ > 0)])
    mneg_grad = np.mean(gradient_[np.where(gradient_ <= 0)])

    # Gradient significatif de la courbe donnée par mean_a
    bornemin = mneg_grad * para_neg
    bornemax = mpos_grad * para_pos
    gradient_S = np.clip(gradient_, bornemin, bornemax)

    # Estimation du TRC
    TRC = 0
    ind_max = np.where(gradient_S == np.max(gradient_S))[0]
    tabmax = np.zeros_like(gradient_S)
    tabmax[ind_max] = 1  # Tableau de 0 et 1 aux indices de gradients max significatifs
    csum = np.cumsum(tabmax)
    diffsum = csum[window:] - csum[:-window]

    indicetrc = np.where(diffsum > 0.5 * window + 1)[0]

    if (np.size(indicetrc) == 0):
        TRC = -1  # Détection des TRC courts

    #######
    else:
        if (window % 2 == 0):
            indicetrc = indicetrc + window / 2
        else:
            indicetrc = indicetrc + (window - 1) / 2

        TRC = np.max(indicetrc)
        #######

    if (np.min(courbelisse) > 0.97 * np.max(courbelisse)): TRC = -1  # Détection des TRC longs

    TRC = TRC / config.DICART_FPS

    return TRC
