import glob
from datetime import date
from os.path import basename, getsize, split, join

import pandas as pd

import config



def read_excel(filename):
    try:
        corr = pd.ExcelFile(filename)
    except OSError:
        print(f"Le fichier {filename} est manquant")
        raise
    sheets = corr.sheet_names
    sheet_1 = pd.read_excel(corr, sheets[0], skiprows=1)
    df_clinic = sheet_1[['fichier', 'TRC clinique']]
    df_clinic = df_clinic.rename(columns={"fichier": "Basename",
                                          "TRC clinique": "TRC_clinic"})
    df_clinic.dropna(axis=0, how='any', inplace=True)

    sheet_2 = pd.read_excel(corr, sheets[1], skiprows=1)
    df_relu = sheet_2[['fichier', 'TRC RELU']]
    df_relu = df_relu.rename(columns={"fichier": "Basename",
                                      "TRC RELU": "TRC_relu"})
    df_relu.dropna(axis=0, how='any', inplace=True)
    df = pd.merge(df_clinic, df_relu, how="left", on="Basename")
    return df


def get_videos(path_pattern):
    videos = glob.glob(path_pattern)
    df_videos = pd.DataFrame({'Fullpath': videos,
                              'Basename': [basename(v) for v in videos],
                              'Patient': [split(split(v)[0])[1] for v in videos],
                              'Filesize': [getsize(v) for v in videos]
                              })
    df_videos = df_videos[df_videos['Filesize'] > 0]
    return df_videos



def mainfile(Excel_name):
    
    #Récupère le fichier de nom Excel_name dans le dossier Ressources
    filename_excel = join(config.RESSOURCES_DIR, Excel_name)
    df_excel = read_excel(filename_excel)
    
    #Récupère le chemin des vidéos se trouvant dans le dossier videos
    path_pattern = join(config.VIDEOS_DIR, '*', '*.mp4')
    df_videos = get_videos(path_pattern)
    
    
    df = pd.merge(df_excel, df_videos, how="inner", on="Basename",
                  sort=True)  # videos exploitables dont on a le trc clinique
    

    
    filename_export = join(config.RESSOURCES_DIR, f'DonneesExploitables_{date.today().strftime("%b-%d-%Y")}.xlsx')
    df.to_excel(filename_export)
    
    return df
    