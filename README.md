# Stage Anne-Sophie Corré
dates: _27/04 au 23/06_

# Structure du dossier

* **config.py**</br>
  Fichier de configuration du programme
  
* **parametres.json**</br>
  Fichier de configuration de l'algorithme
  
* **trc.py**: Estimation du TRC sur une vidéo</br>
  Exemples:</br>
  `python trc.py <nom_ideo>`</br>
  `python trc.py <nom_video> -c parametres.json`

