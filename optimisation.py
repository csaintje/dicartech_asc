# -*- coding: utf-8 -*-
"""
Created on Sat Jun 19 17:05:39 2021

@author: annes
"""

import glob
import numpy as np
from datetime import date
from os.path import basename, getsize, split, join, exists
from sklearn.metrics import mean_squared_error, mean_absolute_error
import pandas as pd
from itertools import product
import ast
import json
from tqdm import tqdm
import asyncio




import config
import prodconsume as pc
import significant_gradient_function as fg
import clean_data as cd


#8Fonction récupérant les moyennes des canaux L et a par Frame
def get_mean_curves(row):

    mean_ = asyncio.run(pc.main_file(row["Fullpath"]))
    mean_l = mean_[0]
    mean_a = mean_[1]
    return mean_l, mean_a


#Fonction déterminant le TRC à partir des données Mean_a et Mean_L du fichier Excel 
#et des paramètres pré instaurés
def trc_aux(row, value0, value1, value2, value3):
    try:
        if "Mean_l" in row:
            fonc = fg.estimation_trc(value0,int(value1),value2,value3,
                                     row["Mean_l"],
                                     row["Mean_a"],
                                     ind_piston=None, para_neg=1)
        else: 
            fonc = fg.estimation_trc(value0,int(value1),value2,value3,
                                     mean_l=None, mean_a=None, ind_piston=None,para_neg=1)
    except:
        return -1
    
    return fonc




def coef_lin(x,y):
    
    z = pd.concat([x, y], axis = 1)
    cov = pd.DataFrame.cov(z, ddof=0).iat[0,1]
    varx = pd.DataFrame.var(x, ddof=0)
    vary = pd.DataFrame.var(y, ddof=0)
    meanx = x.mean()
    meany = y.mean()
    coef = 2*cov/(varx+vary+ (meanx-meany)**2)
    
    return coef





if __name__ == "__main__":
    
    #Récupération du fichier contenant les données
    Excel_name = 'correlation_MIA_HCL_SEB.xlsx'
    filtered_data = cd.mainfile(Excel_name)

    
    #Récupération des moyennes des canaux a et L par frame
    if "Mean_a" not in filtered_data.columns or "Mean_l" not in filtered_data.columns:
        mean_curves = filtered_data.apply(get_mean_curves, axis=1, result_type='expand').T.values

        filtered_data["Mean_l"] = mean_curves[0]
        filtered_data["Mean_a"] = mean_curves[1]
        
        
    filename_export = join(config.RESSOURCES_DIR, f'DonneesExploitables_{date.today().strftime("%b-%d-%Y")}.xlsx')
    filtered_data.to_excel(filename_export)

    relu = filtered_data["TRC_relu"].notna()

    parameters = pd.DataFrame({"Para_pos":[], "Window":[], "Sigma_l":[], "Sigma_a":[],
                       "L2_Clinic_Error": [], "L1_Clinic_Error": [] })


    #Intervalles de recherche des paramètres
    Para_pos = np.linspace(0.1,1,10)
    Window = np.arange(10,31,3)
    Sigma_l = np.linspace(0.1,1,10)
    Sigma_a = np.linspace(0.5,2.5,20)
    
    
    test_dictionary = {"Para_pos": Para_pos, "Window": Window,
                        "Sigma_l": Sigma_l, "Sigma_a": Sigma_a}
    

    
    totallen = len(list(product(*test_dictionary.values())))
     
    for value in tqdm(product(*test_dictionary.values()), total=totallen):

        #Estimation des TRC par l'algorithme
        value = list(value)
        trc_algo = filtered_data.apply(trc_aux, args=value, axis=1)

        data_copy = filtered_data
        data_copy["TRC_algo"] = trc_algo

        #Ne tient compte que des TRC que l'algorithme a réussi à estimer
        data_copy = data_copy[data_copy['TRC_algo'] > 0]

 
        #Détermination du coefficient de Lin entre les TRC estimées par l'algo
        #et leurs TRC cliniques
        x = data_copy["TRC_clinic"]
        y = data_copy["TRC_algo"]
        Lin = coef_lin(x,y)
        

        #Détermination des erreurs en norme L1 et L2
        L2_clinic_algo_error = np.sqrt(mean_squared_error(data_copy["TRC_clinic"], 
                                                          data_copy["TRC_algo"]))
    
        L1_clinic_algo_error = mean_absolute_error(data_copy["TRC_clinic"], 
                                                   data_copy["TRC_algo"])
        

        
        L2_relu_algo_error = np.sqrt(mean_squared_error(data_copy["TRC_relu"][relu],
                                                     data_copy["TRC_algo"][relu]))
        
        L1_relu_algo_error = mean_absolute_error(data_copy["TRC_relu"][relu],
                                                     data_copy["TRC_algo"][relu])

        # Stockage des paramètres, des erreurs et du coefficient de Lin
        # associés dans un DataFrame
        parameters = parameters.append({'Para_pos' : value[0] , 'Window' : value[1], 
                        'Sigma_l' : value[2], 'Sigma_a': value[3], 
                        'L2_Clinic_Algo_Error': L2_clinic_algo_error, 
                        'L1_Clinic_Algo_Error':L1_clinic_algo_error,
                       'L2_relu_Algo_Error': L2_relu_algo_error, 
                       'L1_relu_Algo_Error':L1_relu_algo_error,
                        'Lin': Lin}, ignore_index=True)

        
        
    #Export des paramètres et erreurs associées sous format Excel
    filename_export = join(config.OUTPUT_DIR, f'Recherche_meilleurs_parametres_{date.today().strftime("%b-%d-%Y")}.xlsx')
    parameters.to_excel(filename_export)
    
    
    #Paramètres à stocker selon erreur à minimiser (ou coefficient à maximiser)
    best = parameters["Lin"].argmax()
    best_param = parameters.iloc[best,:]
    best_parameters = {"Para_pos": best_param["Para_pos"], "Window": best_param["Window"], 
                          "Sigma_l": best_param["Sigma_l"], "Sigma_a": best_param["Sigma_a"]}
    

    #Export des paramètres optimaux sous format JSON
    with open(join(config.PARAMETERS_DIR,f'best_parameters_{date.today().strftime("%b-%d-%Y")}.json'), 'w') as fp:
        json.dump(best_parameters, fp)
    














