from os.path import dirname, abspath

PROJECT_DIR = dirname(abspath(__file__))

VIDEOS_DIR = f"{PROJECT_DIR}/videos"
RESSOURCES_DIR = f"{PROJECT_DIR}/ressources"
OUTPUT_DIR = f"{PROJECT_DIR}/output"
PARAMETERS_DIR = f"{PROJECT_DIR}/parameters"


# ACQUISITION   #TODO:A paramétrer en fonction du capteur
DICART_MAX_FRAMES = 100  # Maximum frames in memory
DICART_FPS = 30
DICART_WIDTH = 1920
DICART_HEIGHT = 1080
BOUNDING_BOX_TOP = 98
BOUNDING_BOX_LEFT = 475
BOUNDING_BOX_BOTTOM = None
BOUNDING_BOX_RIGHT = 1480
DECIMATION = 10