# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 09:40:05 2021

@author: annes
"""

import argparse
import asyncio
import json
from os.path import join

import config
import prodconsume as pc
import significant_gradient_function as sg

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="filename for CRT estimation")
    parser.add_argument("--jsonparameters", help="JSON parameters file")
    args = parser.parse_args()

    if args.jsonparameters:
        parameters_path = join(config.PARAMETERS_DIR, '{}.json'.format(args.jsonparameters))
        with open(parameters_path, 'r') as fp:
            parameters = json.load(fp)
    else:
        parameters_path = join(config.PARAMETERS_DIR, 'Reference_optimal_parameters.json')
        with open(parameters_path, 'r') as fp:
            parameters = json.load(fp)

    value = list(parameters.values())

    mean_l, mean_a = asyncio.run(pc.main_file(args.filename))

    #TODO: voir si c'est pas meilleur de mettre **parameters
    trc_estimate = sg.estimation_trc(value[0], int(value[1]), value[2], value[3],
                                     mean_l, mean_a, ind_piston=None, para_neg=1)

    print("Le TRC en secondes est de:", trc_estimate)
